export default {
    state() {
        return {
            user: '',
            loggedIn: false
        };
    },
    mutations: {
        login(state, data) {
            state.user = data;
            state.loggedIn = true;
            //var userData = JSON.parse(localStorage.getItem('user_PNT'));
            //userData.user = data.user;
        },
        logout(state, data) {
            state.user = null;
            state.loggedIn = false;
            state.token = null;
            localStorage.removeItem('auth.strategy');
            localStorage.removeItem('auth._token.local');
            this.$auth.reset();
            document.cookie = 'auth.redirect=%2F; expires=Thu, 01 Jan 2040 00:00:00 UTC; path=/;';
            document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
        },
    },
    actions: {
        login({ commit }, payload) {
            commit('setUser', payload);
        },
        profile({ commit }, costumerid) {
            this.$axios.get(`${process.env.API_URL}/users/${costumerid}`)
                .then((res) => {
                    commit('setUser', res.data.data);
                });
        },
        logout({ commit }) {
            commit('logout');
        }
    }
};