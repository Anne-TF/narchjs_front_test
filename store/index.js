import Vuex from 'vuex'
import Auth from './auth'

new Vuex.Store({
    state: () => ({

    }),
    modules: {
        Auth,
    },
    actions: {
        nuxtServerInit({ commit }, { req }) {
            if (req.session.user) {
                console.log(req.session.user)
                commit('user', req.session.user)
            }
        }
    }
});

export const strict = false;