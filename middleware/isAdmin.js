import { Store } from "vuex";

export default function({ store, req, redirect }) {
    if (process.server && !req) return;
    if (!store.$auth.loggedIn) {
        return redirect('/');
    } else {
        console.log(store.$auth.$state.user);
        if (!store.$auth.$state.user) {
            return redirect('/user');
        }
    }
}